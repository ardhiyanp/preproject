import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegisteruserPage } from '../registeruser/registeruser';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username:any;
  password:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  LogOn(){
    if (this.username == 'prioritas' && this.password == 'admin1234') {
        this.navCtrl.push(HomePage);
    } else {

    }
  }

  SignUp(){
    this.navCtrl.push(RegisteruserPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
