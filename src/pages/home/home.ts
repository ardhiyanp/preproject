import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions, CameraOriginal} from '@ionic-native/camera';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  selectedFilter = null;
  image = '';
  level = 1;
  result: HTMLElement;

  constructor(public navCtrl: NavController, private camera:CameraOriginal) {
   }

  imageLoaded(e) {
    // Grab a reference to the canvas/image
    this.result = e.detail.result;
  }

  captureImage() {
    // Use with a local asset for testing
    // this.image = 'assets/imgs/mallorca.jpg';
    // this.filter(null, 1);
 
    // Real usage with Camera
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true
    }
 
    this.camera.getPicture(options).then((imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.filter(null, 1);
    });
  }
 
  filter(selected, level?) {
    this.selectedFilter = selected;
    this.level = level ? level : 1;
  }
 
  saveImage() {
    if (!this.selectedFilter) {
      // Use the original image!
    } else {
      let canvas = this.result as HTMLCanvasElement;
      // export as dataUrl or Blob!
      let base64 = canvas.toDataURL('image/jpeg', 1.0);
      // Do whatever you want with the result!
    }
  }

}
